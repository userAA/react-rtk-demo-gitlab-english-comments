import axios from 'axios';
import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';

//initial state by users
const initialState = {
    //flag for loading user information
    loading: false,
    //user information
    users: [],
    error: ''
}

// Generates pending, fulfilled and rejected action types
// request to get user information from jsonserver
export const fetchUsers = createAsyncThunk('user/fetchUsers', () => {
    return axios.get('https://jsonplaceholder.typicode.com/users').then(response => response.data)
})

const userSlice = createSlice({
    name: 'user',
    initialState,
    extraReducers: builder => {
        //we start downloading user information from the json server
        builder.addCase(fetchUsers.pending, state => {
            state.loading = true;
        })
        //user information was downloaded successfully from the json server
        builder.addCase(fetchUsers.fulfilled, (state, action) => {
            state.loading = false;
            state.users = action.payload;
            state.error = "";
        })
        //it was not possible to download user information from the json server
        builder.addCase(fetchUsers.rejected, (state, action) => {
            state.loading = false;
            state.users = [];
            state.error = action.error.message;
        })
    }
})

//exporting the producer by users
export default userSlice.reducer