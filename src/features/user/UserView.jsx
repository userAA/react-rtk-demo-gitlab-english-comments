import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { fetchUsers } from './userSlice'

//component by users
export const UserView = () => {
    //we pull out all the information about users from the store using the useSelector hook
    const user = useSelector(state => state.user);

    //using the useEffect and useDispatch hooks, we make a request to get information about users from the json server
    const dispatch = useDispatch();
    useEffect (() => {
        dispatch(fetchUsers())
    }, []);

    return (
        <div>
            <h2>List of Users</h2>
            {/*User information is loaded from jsonserver*/}
            {user.loading && <div>Loading...</div>}
            {/*User information failed to load from jsonserver*/}
            {!user.loading && user.error ? <div>Error: {user.error}</div> : null}
            {/*the downloaded information from the json-server about users is placed in the store 
                and the name of each user is shown */}
            {!user.loading && user.users.length ? (
                <ul>
                    {user.users.map(user => (
                        <li key={user.id}>{user.name}</li>
                    ))}
                </ul>
            ) : null}
        </div>
    )
}