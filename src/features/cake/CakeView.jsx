import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {ordered, restocked} from './cakeSlice';

//cupcake component
export const CakeView = () => {
    //using the useSelector hook, we get the number of cupcakes in the store
    const numOfCakes = useSelector((state) => state.cake.numOfCakes)
    const dispatch = useDispatch();

    return (
        <div>
            <h2>Number of cakes - {numOfCakes} </h2>
            {/*The button to remove the cupcake from the store store*/}
            <button onClick={() =>dispatch(ordered())}>Order cake</button>
            {/*Button for adding five cupcakes to the store*/}
            <button onClick={() =>dispatch(restocked(5))}>Restock cakes</button>
        </div>
    )
}