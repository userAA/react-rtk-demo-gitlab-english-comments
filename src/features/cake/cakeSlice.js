import { createSlice } from '@reduxjs/toolkit';

//the initial number of cupcakes placed in the store
const initialState = {
    numOfCakes: 10
}

const cakeSlice = createSlice({
    name: 'cake',
    initialState,
    reducers: {
        //reducing the number of cupcakes from the store store by one
        ordered: (state) => {
            state.numOfCakes--;
        },
        //adding the number of cupcakes to the store on action.payload
        restocked: (state, action) => {
            state.numOfCakes += action.payload;
        }
    }
})

//exporting the cupcake editor to the store
export default cakeSlice.reducer;
//exporting cupcake actions
export const {ordered, restocked} = cakeSlice.actions;