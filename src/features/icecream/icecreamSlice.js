import {createSlice} from '@reduxjs/toolkit';
import {ordered as cakeOrdered} from '../cake/cakeSlice';

//setting the initial number of ice cream packs in the store
const initialState = {
    numOfIcecreams: 20
}

const icecreamSlice = createSlice({
    name: 'icecream',
    initialState,
    reducers: {
        //reducing the number of ice cream packs in the store
        ordered: (state) => {
            state.numOfIcecreams--;
        },
        //ncreasing the number of ice cream packs on action.payload
        restocked: (state, action) => {
            state.numOfIcecreams += action.payload;
        }        
    },
    //when reducing the number of cupcakes, we reduce the number of packs of ice cream
    extraReducers: builder => {
        builder.addCase(cakeOrdered, state => {
            state.numOfIcecreams--;
        })
    }
})

//we export the producer by packs of ice cream to the store
export default icecreamSlice.reducer;
//exporting actions on ice cream packs
export const {ordered, restocked} = icecreamSlice.actions;