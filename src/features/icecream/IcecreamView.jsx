import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import { ordered, restocked } from './icecreamSlice';

//component by packs of ice cream
export const IcecreamView = () => {
    //status of the number of ice cream packs being added
    const [value, setValue] = React.useState(1);
    //using the useSelector hook, we get the number of ice cream packs in the store
    const numOfIcecreams = useSelector(state => state.icecream.numOfIcecreams); 
    const dispatch = useDispatch();

    return (
        <div>
            <h2>Number of ice creams - {numOfIcecreams} </h2>
            {/*Button to delete a pack of ice cream from the store store*/}
            <button onClick = {() => dispatch(ordered())}>Order ice cream</button>
            {/*setting the number of new ice cream packs */}
            <input
                type='number'
                value={value}
                onChange={e => setValue(parseInt(e.target.value))}
            />
            {/*The button for adding a new number of ice cream packs to the store*/}
            <button onClick = {() => dispatch(restocked(value))}>Restock ice creams</button>
        </div>
    )
}