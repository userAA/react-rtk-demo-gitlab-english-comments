import './App.css'
//we pull out the cupcake component
import { CakeView } from './features/cake/CakeView'
//we pull out the component by packs of ice cream
import { IcecreamView } from './features/icecream/IcecreamView'
//pulling out the component by users
import { UserView } from './features/user/UserView'

function App() {
  return (
    <div className='App'>
      {/*introducing the cupcake component */}
      <CakeView/>
      {/*introducing the ice cream packs component */}
      <IcecreamView/>
      {/*introducing the component by users */}
      <UserView/>
    </div>
  )
}

export default App
