import React from 'react'
import ReactDOM from 'react-dom/client'
import {Provider} from 'react-redux'
import store from './app/store';
import App from './App.jsx'
import './index.css'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>    
      {/*Putting store storage in Provider*/}
    <Provider store={store}>
      {/*Introducing the main component of the project */}
      <App />
    </Provider>
  </React.StrictMode>,
)
