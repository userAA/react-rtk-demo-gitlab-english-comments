import {configureStore} from '@reduxjs/toolkit';
import cakeReducer from '../features/cake/cakeSlice';
import icecreamReducer from '../features/icecream/icecreamSlice';
import userReducer from '../features/user/userSlice';

//creating a store
const store = configureStore({
    reducer : {
        //cupcake producer
        cake: cakeReducer,
        //a producer for ice cream packs
        icecream: icecreamReducer,
        //producer by users
        user: userReducer
    }
})

export default store;